import 'package:flutter/material.dart';

class ColorProvider extends ChangeNotifier {
  bool state = false;

  void changeState() {
    state = !state;
    notifyListeners();
  }
}
