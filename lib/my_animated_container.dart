import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_change_color/color_provider.dart';

class MyAnimatedContainer extends StatefulWidget {
  const MyAnimatedContainer({
    Key? key,
  }) : super(key: key);

  @override
  State<MyAnimatedContainer> createState() => _MyAnimatedContainerState();
}

class _MyAnimatedContainerState extends State<MyAnimatedContainer> {
  @override
  Widget build(BuildContext context) {
    ColorProvider _state = Provider.of<ColorProvider>(context);
    bool changeColor = _state.state;
    return AnimatedContainer(
      duration: const Duration(seconds: 1),
      width: 100,
      height: 100,
      color: changeColor ? Colors.deepPurpleAccent : Colors.red,
    );
  }
}