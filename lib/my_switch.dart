import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_change_color/color_provider.dart';

class MySwitch extends StatefulWidget {
  const MySwitch({Key? key}) : super(key: key);

  @override
  State<MySwitch> createState() => _MySwitchState();
}

class _MySwitchState extends State<MySwitch> {
  @override
  Widget build(BuildContext context) {
    ColorProvider _state = Provider.of<ColorProvider>(context);
    return Switch(
        value: _state.state,
        onChanged: (bool f) {
          _state.changeState();
        });
  }
}
