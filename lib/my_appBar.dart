import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_change_color/color_provider.dart';


class MyAppBar extends StatefulWidget {
  const MyAppBar({Key? key}) : super(key: key);

  @override
  State<MyAppBar> createState() => _MyAppBarState();
}

 class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    ColorProvider _state = Provider.of<ColorProvider>(context);
    bool changeColor = _state.state;
    return AppBar(
      elevation: 0,
      backgroundColor: changeColor ? Colors.greenAccent : Colors.yellow,
      title: Text(
        'Practice Provider',
        style: TextStyle(
          fontSize: 30,
          color: changeColor ? Colors.deepPurpleAccent : Colors.red,
        ),
      ),
      centerTitle: true,
    );
  }
}