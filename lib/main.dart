import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_change_color/color_provider.dart';
import 'package:provider_change_color/my_animated_container.dart';
import 'package:provider_change_color/my_appBar.dart';
import 'package:provider_change_color/my_switch.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider<ColorProvider>.value(value: ColorProvider()),
        ],
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: const PreferredSize(
              preferredSize: Size.fromHeight(kToolbarHeight),
              child: MyAppBar()),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                MyAnimatedContainer(),
                SizedBox(
                  height: 15,
                ),
                MySwitch(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


